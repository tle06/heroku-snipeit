# 2022.01.15

FROM linuxserver/snipe-it:5.3.7
LABEL maintainer="tle@tlnk.fr"

USER abc
COPY root/ /
USER root

CMD sed -i -e 's/$PORT/'"$PORT"'/g' /config/nginx/site-confs/default && nginx -g 'daemon off;'

LABEL org.label-schema.name="tlnk.fr"
LABEL org.label-schema.description="homer application"
LABEL org.label-schema.url="https://console.tlnk.fr"
LABEL org.label-schema.vendor="TLNK"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vcs-url="https://gitlab.com/tle06/argo-applications"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.docker.cmd=""