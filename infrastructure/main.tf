/* -------------------------------------------------------------------------- */
/*                     Terraform state with gitlab backend                    */
/* -------------------------------------------------------------------------- */

terraform {
  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5

  }
}

/* -------------------------------------------------------------------------- */
/*                                   locals                                   */
/* -------------------------------------------------------------------------- */

locals {
  heroku = {
    region = "eu"
  }
  app = {
    name               = "snipeit"
    domain             = "tlnk.fr"
    azure_zone_rg_name = "TF_PROD_DNS_PUBLIC"

    prod = {
      heroku_formation_size = "free"
      dns_record_name       = "inventory"

    }

    stage = {
      heroku_formation_size = "free"
      dns_record_name       = "inventory.stage"
    }
  }
}

/* -------------------------------------------------------------------------- */
/*                                  Database                                  */
/* -------------------------------------------------------------------------- */

data "scaleway_rdb_instance" "database" {
  name = "prod-database01"
}

resource "scaleway_rdb_database" "database" {
  instance_id = data.scaleway_rdb_instance.database.id
  name        = "${var.terraform-name-prefix}-${local.environment}-${local.app.name}"
}

resource "random_password" "database_user_password" {
  length  = 32
  special = true
}

resource "scaleway_rdb_user" "user" {
  instance_id = data.scaleway_rdb_instance.database.id
  name        = "${var.terraform-name-prefix}-${local.environment}-${local.app.name}"
  password    = random_password.database_user_password.result
  is_admin    = false
}

resource "scaleway_rdb_privilege" "privilege" {
  instance_id   = data.scaleway_rdb_instance.database.id
  user_name     = scaleway_rdb_user.user.name
  database_name = scaleway_rdb_database.database.name
  permission    = "all"
}

/* -------------------------------------------------------------------------- */
/*                                   Heroku                                   */
/* -------------------------------------------------------------------------- */

resource "heroku_app" "app" {
  name   = "${local.environment}-${local.app.name}"
  region = local.heroku.region

}

resource "heroku_app_config_association" "app" {
  app_id = heroku_app.app.id
  sensitive_vars = {
    ENV                      = local.environment
    APP_DEBUG                = "false"
    APP_KEY                  = var.SNIPEIT_API_KEY
    APP_TIMEZONE             = "UTC"
    APP_LOCALE               = "en"
    MYSQL_PORT_3306_TCP_ADDR = data.scaleway_rdb_instance.database.endpoint_ip
    MYSQL_PORT_3306_TCP_PORT = data.scaleway_rdb_instance.database.endpoint_port
    MYSQL_DATABASE           = scaleway_rdb_database.database.name
    MYSQL_USER               = scaleway_rdb_user.user.name
    MYSQL_PASSWORD           = random_password.database_user_password.result
    PGID                     = 1000
    PUID                     = 1000
    NGINX_APP_URL            = "localhost"

  }
}

resource "heroku_formation" "app_formation" {
  count    = local.app[local.environment].heroku_formation_size == "free" ? 0 : 1
  app      = heroku_app.app.name
  type     = "web"
  size     = local.app[local.environment].heroku_formation_size
  quantity = 1
}


resource "heroku_domain" "app_custom_domain" {
  app      = heroku_app.app.name
  hostname = "${local.app[local.environment].dns_record_name}.${local.app.domain}"
}

/* -------------------------------------------------------------------------- */
/*                                     DNS                                    */
/* -------------------------------------------------------------------------- */

resource "azurerm_dns_cname_record" "app_record" {
  name                = local.app[local.environment].dns_record_name
  zone_name           = local.app.domain
  resource_group_name = local.app.azure_zone_rg_name
  ttl                 = 300
  record              = heroku_domain.app_custom_domain.cname
}